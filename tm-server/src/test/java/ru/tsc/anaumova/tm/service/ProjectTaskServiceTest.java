package ru.tsc.anaumova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.anaumova.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.anaumova.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.tsc.anaumova.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.anaumova.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.anaumova.tm.configuration.ContextConfiguration;
import ru.tsc.anaumova.tm.dto.model.ProjectDTO;
import ru.tsc.anaumova.tm.dto.model.TaskDTO;
import ru.tsc.anaumova.tm.dto.model.UserDTO;
import ru.tsc.anaumova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.anaumova.tm.exception.field.EmptyIdException;
import ru.tsc.anaumova.tm.exception.field.EmptyUserIdException;

public class ProjectTaskServiceTest {

    @NotNull
    private IProjectTaskServiceDTO projectTaskService;

    @NotNull
    private IUserServiceDTO userService;

    @NotNull
    private IProjectServiceDTO projectService;

    @NotNull
    private ITaskServiceDTO taskService;

    private String USER_ID;

    private String PROJECT_ID;

    private String TASK_ID;

    @Before
    public void init() {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ContextConfiguration.class);
        projectTaskService = context.getBean(IProjectTaskServiceDTO.class);
        userService = context.getBean(IUserServiceDTO.class);
        projectService = context.getBean(IProjectServiceDTO.class);
        taskService = context.getBean(ITaskServiceDTO.class);
        @NotNull final UserDTO user = userService.create("user", "user");
        USER_ID = user.getId();
        @NotNull final ProjectDTO project = projectService.create(USER_ID, "project");
        PROJECT_ID = project.getId();
        @NotNull final TaskDTO task = taskService.create(USER_ID, "task");
        TASK_ID = task.getId();
    }

    @After
    public void end() {
        taskService.clear(USER_ID);
        projectService.clear(USER_ID);
        userService.removeByLogin("user");
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectTaskService.bindTaskToProject("", PROJECT_ID, TASK_ID));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, "", TASK_ID));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, ""));
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, "not_task_id"));

        projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, TASK_ID);
        @NotNull final TaskDTO task = taskService.findOneById(TASK_ID);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(PROJECT_ID, task.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectTaskService.unbindTaskFromProject("", PROJECT_ID, TASK_ID));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, "", TASK_ID));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, ""));
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, "not_task_id"));
        projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, TASK_ID);
        @NotNull final TaskDTO task = taskService.findOneById(TASK_ID);
        Assert.assertNull(task.getProjectId());
    }

}