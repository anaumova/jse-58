package ru.tsc.anaumova.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.anaumova.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.anaumova.tm.api.service.IAuthService;
import ru.tsc.anaumova.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.anaumova.tm.dto.model.SessionDTO;
import ru.tsc.anaumova.tm.dto.model.UserDTO;
import ru.tsc.anaumova.tm.dto.request.UserLoginRequest;
import ru.tsc.anaumova.tm.dto.request.UserLogoutRequest;
import ru.tsc.anaumova.tm.dto.request.UserProfileRequest;
import ru.tsc.anaumova.tm.dto.response.UserLoginResponse;
import ru.tsc.anaumova.tm.dto.response.UserLogoutResponse;
import ru.tsc.anaumova.tm.dto.response.UserProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Getter
@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.anaumova.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    @NotNull
    @Autowired
    private IUserServiceDTO userService;

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        @NotNull final IAuthService authService = getAuthService();
        @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        check(request);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserProfileRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final UserDTO user = getUserService().findOneById(session.getUserId());
        return new UserProfileResponse(user);
    }

}