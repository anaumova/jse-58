package ru.tsc.anaumova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.anaumova.tm.api.repository.model.IProjectRepository;
import ru.tsc.anaumova.tm.enumerated.Sort;
import ru.tsc.anaumova.tm.model.Project;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public List<Project> findAll() {
        @NotNull final String jpql = "SELECT m FROM Project m";
        return entityManager.createQuery(jpql, Project.class).getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull String userId) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull String userId, @NotNull Sort sort) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.userId = :userId ORDER BY m."
                + getSortType(sort.getComparator());
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull String userId, @NotNull Comparator<Project> comparator) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.userId = :userId ORDER BY m." + getSortType(comparator);
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull String id) {
        return entityManager.find(Project.class, id);
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty() || id.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull String id) {
        Optional<Project> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        Optional<Project> model = Optional.ofNullable(findOneById(userId, id));
        model.ifPresent(this::remove);
    }

    @Override
    public void clear() {
        @NotNull final List<Project> projects = findAll();
        projects.forEach(this::remove);
    }

    @Override
    public void clear(@NotNull String userId) {
        if (userId.isEmpty()) return;
        @NotNull final List<Project> projects = findAll(userId);
        projects.forEach(this::remove);
    }

    @Override
    public long getCount() {
        @NotNull final String jpql = "SELECT COUNT(e) FROM Project e";
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

    @Override
    public long getCount(@NotNull String userId) {
        @NotNull final String jpql = "SELECT COUNT(e) FROM Project e WHERE e.userId = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull String id) {
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM Project m WHERE m.id = :id";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty() || id.isEmpty()) return false;
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM Project m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult();
    }

}