package ru.tsc.anaumova.tm.configuration;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.tsc.anaumova.tm.api.service.IDatabaseProperty;
import ru.tsc.anaumova.tm.dto.model.ProjectDTO;
import ru.tsc.anaumova.tm.dto.model.TaskDTO;
import ru.tsc.anaumova.tm.dto.model.UserDTO;
import ru.tsc.anaumova.tm.listener.EntityListener;
import ru.tsc.anaumova.tm.model.Project;
import ru.tsc.anaumova.tm.model.Task;
import ru.tsc.anaumova.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan("ru.tsc.anaumova.tm")
public class ContextConfiguration {

    @Bean
    @NotNull
    public EntityManagerFactory entityManagerFactory(
            @NotNull final IDatabaseProperty databaseProperty,
            @NotNull final EntityListener entityListener
    ) {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, databaseProperty.getDatabaseDriver());
        settings.put(Environment.URL, databaseProperty.getDatabaseUrl());
        settings.put(Environment.USER, databaseProperty.getDatabaseUser());
        settings.put(Environment.PASS, databaseProperty.getDatabasePassword());
        settings.put(Environment.DIALECT, databaseProperty.getDatabaseDialect());
        settings.put(Environment.HBM2DDL_AUTO, databaseProperty.getDatabaseHbm2ddlAuto());
        settings.put(Environment.SHOW_SQL, databaseProperty.getDatabaseShowSql());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, databaseProperty.getUseSecondLevelCache());
        settings.put(Environment.USE_QUERY_CACHE, databaseProperty.getUseQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, databaseProperty.getUseMinimalPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, databaseProperty.getCacheRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, databaseProperty.getCacheProviderConfig());
        settings.put(Environment.CACHE_REGION_FACTORY, databaseProperty.getCacheRegionFactory());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(Task.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        EntityManagerFactory factory = metadata.getSessionFactoryBuilder().build();
        initListeners(factory, entityListener);
        return factory;
    }

    @Bean
    @NotNull
    @Scope("prototype")
    public EntityManager getEntityManager(@NotNull final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

    private void initListeners(@NotNull final EntityManagerFactory factory, @NotNull final EntityListener entityListener) {
        @NotNull final SessionFactoryImpl sessionFactory = factory.unwrap(SessionFactoryImpl.class);
        @NotNull final EventListenerRegistry registry = sessionFactory.getServiceRegistry().getService(EventListenerRegistry.class);
        registry.getEventListenerGroup(EventType.POST_INSERT).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.POST_DELETE).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.PRE_INSERT).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.PRE_DELETE).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.PRE_UPDATE).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.POST_LOAD).appendListener(entityListener);
    }

}
