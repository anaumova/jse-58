package ru.tsc.anaumova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.anaumova.tm.api.repository.model.IProjectRepository;
import ru.tsc.anaumova.tm.api.service.model.IProjectTaskService;
import ru.tsc.anaumova.tm.api.service.model.ITaskService;
import ru.tsc.anaumova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.anaumova.tm.exception.field.EmptyIdException;
import ru.tsc.anaumova.tm.exception.field.EmptyUserIdException;
import ru.tsc.anaumova.tm.model.Project;
import ru.tsc.anaumova.tm.model.Task;

import java.util.List;

@Service
public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @Override
    public void bindTaskToProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        @Nullable final Project project = projectRepository.findOneById(projectId);
        task.setProject(project);
        taskService.update(task);
    }

    @Override
    public void unbindTaskFromProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(null);
        taskService.update(task);
    }

    @Override
    public void removeProjectById(@NotNull String userId, @NotNull String projectId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final List<Task> taskList = taskService.findAllByProjectId(userId, projectId);
        taskList.forEach(taskService::remove);
    }

}