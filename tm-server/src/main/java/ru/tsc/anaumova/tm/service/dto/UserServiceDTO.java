package ru.tsc.anaumova.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.anaumova.tm.api.repository.dto.IUserRepositoryDTO;
import ru.tsc.anaumova.tm.api.service.IPropertyService;
import ru.tsc.anaumova.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.anaumova.tm.dto.model.UserDTO;
import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.exception.entity.UserNotFoundException;
import ru.tsc.anaumova.tm.exception.field.*;
import ru.tsc.anaumova.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Optional;

@Service
public class UserServiceDTO extends AbstractServiceDTO<UserDTO, IUserRepositoryDTO> implements IUserServiceDTO {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Override
    protected IUserRepositoryDTO getRepository() {
        return context.getBean(IUserRepositoryDTO.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final IUserRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final UserDTO user = new UserDTO();
        try {
            user.setLogin(login);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    ) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        @NotNull final IUserRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final UserDTO user = new UserDTO();
        try {
            user.setLogin(login);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            user.setEmail(email);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final Role role
    ) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final IUserRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final UserDTO user = new UserDTO();
        try {
            user.setLogin(login);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            if (role == null) user.setRole(Role.USUAL);
            else user.setRole(role);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmptyEmailException();
        @NotNull final IUserRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        remove(user);
    }

    @NotNull
    @Override
    public UserDTO setPassword(@NotNull final String id, @NotNull final String password) {
        if (id.isEmpty()) throw new EmptyIdException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final UserDTO user = findOneById(id);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        update(user);
        return user;
    }

    @NotNull
    @Override
    public UserDTO updateUser(
            @NotNull final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final UserDTO user = findOneById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    public void lockUserByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        final UserDTO user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(true);
        update(user);
    }

    @Override
    public void unlockUserByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        final UserDTO user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(false);
        update(user);
    }

}