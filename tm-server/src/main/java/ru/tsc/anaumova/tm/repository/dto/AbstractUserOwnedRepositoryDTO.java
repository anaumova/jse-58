package ru.tsc.anaumova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.anaumova.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.tsc.anaumova.tm.comparator.CreatedComparator;
import ru.tsc.anaumova.tm.comparator.DateBeginComparator;
import ru.tsc.anaumova.tm.comparator.StatusComparator;
import ru.tsc.anaumova.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.Comparator;

@Repository
@Scope("prototype")
public abstract class AbstractUserOwnedRepositoryDTO<M extends AbstractUserOwnedModelDTO> extends AbstractRepositoryDTO<M>
        implements IUserOwnedRepositoryDTO<M> {

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else if (comparator == DateBeginComparator.INSTANCE) return "status";
        else return "name";
    }

    public void add(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.persist(model);
    }

    public void update(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.remove(model);
    }

}