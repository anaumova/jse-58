package ru.tsc.anaumova.tm.listener.task;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.anaumova.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.anaumova.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.anaumova.tm.dto.model.TaskDTO;
import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.enumerated.Status;
import ru.tsc.anaumova.tm.listener.AbstractListener;
import ru.tsc.anaumova.tm.util.DateUtil;

import java.util.List;

@Getter
@Component
public abstract class AbstractTaskListener extends AbstractListener {

    @Autowired
    private ITaskEndpoint taskEndpoint;

    @Autowired
    private IProjectTaskEndpoint projectTaskEndpoint;

    protected void renderTasks(@NotNull final List<TaskDTO> tasks) {
        int index = 1;
        for (TaskDTO task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void showTask(@NotNull final TaskDTO task) {
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(task.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(task.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(task.getDateEnd()));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}