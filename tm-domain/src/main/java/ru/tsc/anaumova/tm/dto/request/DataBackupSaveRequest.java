package ru.tsc.anaumova.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataBackupSaveRequest extends AbstractUserRequest {

    public DataBackupSaveRequest(@Nullable String token) {
        super(token);
    }

}