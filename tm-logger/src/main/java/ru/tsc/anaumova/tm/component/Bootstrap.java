package ru.tsc.anaumova.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.anaumova.tm.api.service.IReceiverService;
import ru.tsc.anaumova.tm.listener.LogListener;

@Component
public class Bootstrap {

    @NotNull
    @Autowired
    private IReceiverService receiverService;

    @NotNull
    @Autowired
    private LogListener logListener;

    @SneakyThrows
    public void init() {
        receiverService.init(logListener);
    }

}